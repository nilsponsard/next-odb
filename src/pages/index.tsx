import * as React from 'react';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { Button } from '@mui/material';
import { styled } from '@mui/material/styles';
import { ImportCRMAX } from '@/parser/crmax';
import { OpenFile } from '@/parser/openfile';
import { ChartsData } from '@/parser/commonDataType';
import Charts from '@/components/Charts';

const Input = styled(`input`)({
  display: `none`,
});

export default function Home() {
  const [mode, setMode] = React.useState(`crmax`);
  const [data, setData] = React.useState([] as ChartsData);
  console.log(data);

  return (
    <div>
      <FormControl component="fieldset">
        <FormLabel component="legend">File Type</FormLabel>
        <RadioGroup
          aria-label="gender"
          defaultValue="crmax"
          name="radio-buttons-group"
          onChange={(ev) => {
            setMode(ev.target.value);
          }}
        >
          <FormControlLabel value="crmax" control={<Radio />} label="CR Max" />
        </RadioGroup>
        <label htmlFor="contained-button-file">
          <Input
            accept="*"
            id="contained-button-file"
            type="file"
            onChange={(ev) => {
              let fn;
              switch (mode) {
                default:
                case `crmax`:
                  fn = ImportCRMAX;
                  break;
              }
              OpenFile(ev.target, fn, (da) => setData(da));
            }}
          />
          <Button variant="contained" component="span">
            Upload
          </Button>
        </label>
      </FormControl>
      <Charts data={data} />
    </div>
  );
}
