import pako from 'pako';
import { ChartsData } from './commonDataType';

export function ImportCRMAX(
  input: string,
  callback: (data: ChartsData) => any,
) {
  const buf = Buffer.from(input, `base64`);

  //   let raw = atob(input);

  //   let arr = raw.split('').map((val) => {
  //     return val.charCodeAt(0);
  //   });

  //   const binData = new Uint8Array(arr);

  const data = pako.inflate(buf);

  const out: ChartsData = [];

  let str = ``;
  // decode data
  const decoder = new TextDecoder(`utf-8`);

  str = decoder.decode(data);

  const json = JSON.parse(str) as Array<{
    frameList: Array<{
      dispalyType: number;
      id: number;
      index: number;
      maxRangValue: number;
      minRagValue: number;
      nameString: string;
      rangString: string;
      unitString: string;
      valueString: string;
    }>;
    time: string;
  }>;

  console.log(json);

  if (!json.length || json.length < 1) {
    console.error(`inalid file`);
    return -1; //invalid data
  }

  for (let j = 1; j < json[0].frameList.length; ++j) {
    const frame = json[0].frameList[j];
    const value = parseFloat(frame.valueString);
    if (Number.isNaN(value)) out[j] = undefined;
    else
      out[j] = { name: frame.nameString, unit: frame.unitString, values: [] };
  }

  for (let i = 0; i < json.length; ++i) {
    for (let j = 0; j < json[0].frameList.length; ++j) {
      out[j]?.values.push(parseFloat(json[i].frameList[j].valueString));
    }
  }
  callback(out);
}
