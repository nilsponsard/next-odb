import { ChartData, ChartsData as ChartsData } from './commonDataType';

export function OpenFile(
  el: HTMLInputElement,
  callback: (str: string, callback: (data: ChartsData) => any) => any,
  dataCallback: (data: ChartsData) => any,
) {
  if (el.files) {
    if (el.files.length > 0) {
      const file = el.files[0];
      if (file) {
        const reader = new FileReader();
        reader.addEventListener(`load`, (e) => {
          if (e.target?.result) {
            const result = e.target.result.toString();
            callback(result, dataCallback);
          }
        });
        reader.readAsText(file);
      }
    }
  }
}
