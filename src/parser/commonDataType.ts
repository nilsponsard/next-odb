export interface ChartData {
  name: string;
  unit: string;
  values: Array<number>;
}

export type ChartsData = Array<ChartData | undefined>;
