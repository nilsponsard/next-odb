import { ChartData } from '@/parser/commonDataType';

import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  Brush,
} from 'recharts';

import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { useState } from 'react';

export default function Chart({
  data,
  brush,
}: {
  data: ChartData;
  brush?: boolean;
}) {
  const [visible, setVisible] = useState(true);

  const Label = (
    <FormGroup
      onChange={() => {
        setVisible(!visible);
        console.log(visible);
      }}
    >
      <FormControlLabel
        control={<Checkbox />}
        label={data.name}
        checked={visible}
      />
    </FormGroup>
  );

  if (!visible) {
    return <>{Label}</>;
  }

  const points = [];

  for (let i = 0; i < data.values.length; i++) {
    const d = data.values[i];
    const obj = {} as any;
    obj[`name`] = i.toString();
    obj[data.unit] = d;
    points.push(obj);
  }

  return (
    <>
      {Label}
      <ResponsiveContainer width="100%" height={200} key={data.name}>
        <LineChart
          width={500}
          height={200}
          data={points}
          syncId="anyId"
          margin={{
            top: 10,
            right: 30,
            left: 0,
            bottom: 0,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Line
            type="monotone"
            dataKey={data.unit}
            stroke="#8884d8"
            fill="#8884d8"
          />
          {brush && <Brush dataKey="name" height={30} stroke="#8884d8" />}
        </LineChart>
      </ResponsiveContainer>
    </>
  );
}
