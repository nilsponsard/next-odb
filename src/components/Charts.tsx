import { ChartsData } from '@/parser/commonDataType';
import React from 'react';
import { Brush } from 'recharts';
import Chart from './Chart';

export default function Charts({ data }: { data: ChartsData }) {
  if (data.length == 0) {
    return <></>;
  }
  console.log(data);
  const charts: Array<React.ReactElement> = [];

  let b = true;

  data.forEach((d) => {
    if (d) {
      charts.push(<Chart key={d.name} data={d} brush={b} />);
      b = false;
    }
  });

  return <div style={{ width: `100%` }}>{charts}</div>;
}
